package controllers

import (
	"github.com/gofiber/fiber/v2"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/Petrakan/fiber_todo/database"
)

//Описываес структуру ответа
type Response struct {
	Massage string      `json:"message"`
	Success bool        `json:"success"`
	Data    interface{} `json:"data"`
}

var response Response

// Описываем структуру задач
type Todo struct {
	gorm.Model
	Title     string `json:"title"`
	Completed bool   `json:"completed"`
}

//Контроллер обработки запроза списка всех задач
func GetTodos(c *fiber.Ctx) error {
	db := database.DBConn
	todos := new([]Todo)
	db.Find(&todos)
	if len(*todos) == 0 {
		response.Massage = "У вас пока не назначено никаких дел"
		response.Success = false
		response.Data = nil
		return c.Status(fiber.StatusNotFound).JSON(response)
	}
	response.Massage = "All ok"
	response.Success = true
	response.Data = &todos
	return c.Status(fiber.StatusOK).JSON(response)
}

//Контроллер создания задачи
func CreateTodo(c *fiber.Ctx) error {
	db := database.DBConn
	todo := new(Todo)
	err := c.BodyParser(todo)
	if err != nil {
		response.Massage = "Не удалось создать заметку. Неверные входные данные"
		response.Success = false
		response.Data = nil
		return c.Status(500).JSON(response)
	}
	db.Create(&todo)
	response.Massage = "Заметка успешно создана"
	response.Success = true
	response.Data = *todo
	return c.Status(fiber.StatusCreated).JSON(response)
}

//контроллер обработки запроса одной задачи по идентификатору
func GetTodo(c *fiber.Ctx) error {
	id := c.Params("id")
	db := database.DBConn
	var todo Todo
	db.Find(&todo, id)
	if todo.Title == "" {
		response.Massage = "Не удалось найти заметку с указанным ID"
		response.Success = false
		response.Data = nil
		return c.Status(500).JSON(response)
	}
	response.Massage = "Заметка успешно найдена"
	response.Success = true
	response.Data = todo
	return c.Status(fiber.StatusCreated).JSON(response)
}

// //Контроллер обрабоки обновления задачи
func UpdateTodo(c *fiber.Ctx) error {
	// задаем id
	id := c.Params("id")
	db := database.DBConn
	var oldTodo Todo
	db.First(&oldTodo, id)

	if oldTodo.Title == "" {
		response.Massage = "Не удалось найти заметку с указанным ID"
		response.Success = false
		response.Data = nil
		return c.Status(500).JSON(response)
	}
	// Описываем структуру запроса
	type Request struct {
		Title     *string `json:"title"`
		Completed *bool   `json:"completed"`
	}

	var newTodo Request
	err := c.BodyParser(&newTodo)
	if err != nil {
		response.Massage = "Не удалось создать заметку. Неверные входные данные"
		response.Success = false
		response.Data = nil
		return c.Status(500).JSON(response)
	}

	if newTodo.Title != nil && newTodo.Completed != nil {
		oldTodo.Title = *newTodo.Title
		oldTodo.Completed = *newTodo.Completed
	}

	db.Save(&oldTodo)

	response.Massage = "Заметка успешно обновлена"
	response.Success = true
	response.Data = oldTodo
	return c.Status(fiber.StatusOK).JSON(response)
}

// Контроллер обработки удаления задачи
func DeleteTodo(c *fiber.Ctx) error {
	// Получаем id
	id := c.Params("id")
	db := database.DBConn

	var todo Todo
	db.First(&todo, id)

	if todo.Title == "" {
		response.Massage = "Не удалось найти заметку с указанным ID"
		response.Success = false
		response.Data = nil
		return c.Status(500).JSON(response)
	}

	db.Delete(&todo)

	response.Massage = "Заметка успешно удалена"
	response.Success = true
	response.Data = nil
	return c.Status(fiber.StatusOK).JSON(response)
}

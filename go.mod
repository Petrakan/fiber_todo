module gitlab.com/Petrakan/fiber_todo

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/armon/consul-api v0.0.0-20180202201655-eb2c6b5be1b6 // indirect
	github.com/coreos/etcd v3.3.10+incompatible // indirect
	github.com/coreos/go-etcd v2.0.0+incompatible // indirect
	github.com/gofiber/fiber/v2 v2.14.0
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/klauspost/compress v1.13.1 // indirect
	github.com/mattn/go-sqlite3 v1.14.7 // indirect
	github.com/spf13/viper v1.8.1 // indirect
	github.com/ugorji/go/codec v0.0.0-20181204163529-d75b2dcb6bc8 // indirect
	github.com/valyala/fasthttp v1.28.0 // indirect
	github.com/xordataexchange/crypt v0.0.3-0.20170626215501-b2862e3d0a77 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)

package database

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/spf13/viper"
)

var (
	DBConn *gorm.DB
)

//Функция подключения базы данных
func InitDatabase() {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Europe/Moscow", viper.GetString("db.host"), viper.GetString("db.username"), os.Getenv("DB_PASSWORD"), viper.GetString("db.dbname"), viper.GetString("db.port"))
	var err error
	DBConn, err = gorm.Open("postgres", dsn)
	if err != nil {
		panic("Не удалось подключиться к базе данных")
	}
	fmt.Println("Соединение с базой данных установленно")
}

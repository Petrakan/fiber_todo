package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	"gitlab.com/Petrakan/fiber_todo/controllers"
	"gitlab.com/Petrakan/fiber_todo/database"
	"gitlab.com/Petrakan/fiber_todo/routes"
)

func main() {
	//Запускаем конфиги
	if err := initConfig(); err != nil {
		panic(err)
	}
	// Создаем экземпляр приложения Fiber
	app := fiber.New()
	// Подключаем логер
	app.Use(logger.New())
	//Добавляем чтение из .env
	if err := godotenv.Load(); err != nil {
		log.Fatalf("Ошибка чтения .env %v", err)
	} else {
		fmt.Println("Переменные из .env получены.")
	}

	// Инициализируем базу данных
	database.InitDatabase()
	defer database.DBConn.Close()
	// Автомиграции
	database.DBConn.AutoMigrate(&controllers.Todo{})

	// Подключаем роуты с хэндлерами
	setupRoutes(app)

	// Настройки порта для деплоя на heroku
	port := os.Getenv("PORT")
	if port == "" {
		port = ":8080"
	}
	if err := app.Listen(port); err != nil {
		panic(err)
	}
}

//Функция инициализации конфига
func initConfig() error {
	viper.AddConfigPath("config")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}

// Описываем хэндлеры
func setupRoutes(app *fiber.App) {

	type Response struct {
		Message string `json:"massage"`
		Success bool   `json:"success"`
	}
	var response Response

	app.Get("/", func(c *fiber.Ctx) error {
		response.Message = "You are at homepage endpoint"
		response.Success = true
		return c.Status(fiber.StatusOK).JSON(response)
	})

	//Обьявляем группу апи
	api := app.Group("/api")

	//Обработка ответов в /api
	api.Get("", func(c *fiber.Ctx) error {
		response.Message = "You are at api endpoint"
		response.Success = true
		return c.Status(fiber.StatusOK).JSON(response)
	})

	routes.TodoRoute(api.Group("/todos"))
}

# Start from golang base image
FROM golang:1.16-alpine

# ENV GO111MODULE=on

# Install git.
# Git is required for fetching the dependencies.
RUN apk --no-cache add make git gcc libtool musl-dev ca-certificates dumb-init

# Set the current working directory inside the container 
RUN mkdir /fiber_todo
WORKDIR /fiber_todo

# Copy go mod and sum files 
COPY go.mod go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and the go.sum files are not changed 
RUN go mod download 

# Copy the source from the current directory to the working Directory inside the container 
COPY . .

# Build the Go app
RUN go build -o fiber_todo .

# Start a new stage from scratch
# FROM alpine:latest
# RUN apk --no-cache add ca-certificates

# WORKDIR /root/

# Copy the Pre-built binary file from the previous stage. Observe we also copied the .env file
# COPY --from=builder /fiber_todo/main .
# COPY --from=builder /fiber_todo/.env .       

# Expose port 8080 to the outside world
EXPOSE 8080
#Command to run the executable
CMD ["/fiber_todo/fiber_todo"]
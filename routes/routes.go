package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/Petrakan/fiber_todo/controllers"
)

func TodoRoute(route fiber.Router) {
	route.Get("", controllers.GetTodos)          // Запрос всегоь списка задач
	route.Post("", controllers.CreateTodo)       // Создание задачи
	route.Put("/:id", controllers.UpdateTodo)    // обновление задачи
	route.Delete("/:id", controllers.DeleteTodo) // удаление задачи
	route.Get("/:id", controllers.GetTodo)       // запрос отдельной задачи по id
}
